<?php

namespace Drupal\fallback_media\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FallbackEntityForm.
 */
class FallbackEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $fallback_entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $fallback_entity->label(),
      '#description' => $this->t("Label for the Fallback entity."),
      '#required' => TRUE,
    ];

    $form['fallback'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'media',
      '#title' => $this->t('Fallback Entity'),
      '#default_value' => ($fallback_entity->getFallback()) ? $this->entityTypeManager->getStorage('media')->load($fallback_entity->getFallback()) : '',
      '#description' => $this->t("The Fallback entity to use."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $fallback_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\fallback_media\Entity\FallbackEntity::load',
      ],
      '#disabled' => !$fallback_entity->isNew(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $fallback_entity = $this->entity;
    $status = $fallback_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Fallback entity.', [
          '%label' => $fallback_entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Fallback entity.', [
          '%label' => $fallback_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($fallback_entity->toUrl('collection'));
  }

}
