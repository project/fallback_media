<?php

namespace Drupal\fallback_media;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Fallback entity entities.
 */
class FallbackEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Fallback entity');
    $header['id'] = $this->t('Machine name');
    $header['fallback'] = $this->t('Fallback to');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $fallbackEntity = \Drupal::entityTypeManager()->getStorage('media')->load($entity->getFallback());
    $row['fallback'] = ($fallbackEntity) ? $fallbackEntity->label() : "Not selected";
    return $row + parent::buildRow($entity);
  }

}
