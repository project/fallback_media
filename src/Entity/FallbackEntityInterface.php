<?php

namespace Drupal\fallback_media\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Fallback entity entities.
 */
interface FallbackEntityInterface extends ConfigEntityInterface {

  /**
   * Get the Fallback entity.
   *
   * @return int
   */
  public function getFallback();

  /**
   * Set the Fallback entity.
   *
   * @param int $fallback  The Fallback entity.
   *
   * @return self
   */
  public function setFallback(int $fallback);

}
