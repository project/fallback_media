<?php

namespace Drupal\fallback_media\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Fallback entity entity.
 *
 * @ConfigEntityType(
 *   id = "fallback_entity",
 *   label = @Translation("Fallback entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\fallback_media\FallbackEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\fallback_media\Form\FallbackEntityForm",
 *       "edit" = "Drupal\fallback_media\Form\FallbackEntityForm",
 *       "delete" = "Drupal\fallback_media\Form\FallbackEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\fallback_media\FallbackEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "fallback_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "fallback",
 *     "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/fallback_entity/{fallback_entity}",
 *     "add-form" = "/admin/structure/fallback_entity/add",
 *     "edit-form" = "/admin/structure/fallback_entity/{fallback_entity}/edit",
 *     "delete-form" = "/admin/structure/fallback_entity/{fallback_entity}/delete",
 *     "collection" = "/admin/structure/fallback_entity"
 *   }
 * )
 */
class FallbackEntity extends ConfigEntityBase implements FallbackEntityInterface {

  /**
   * The Fallback entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Fallback entity label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Fallback entity.
   *
   * @var int
   */
  protected $fallback;

  /**
   * {@inheritdoc}
   */
  public function getFallback()
  {
    return $this->fallback;
  }

  /**
   * {@inheritdoc}
   */
  public function setFallback(int $fallback)
  {
    $this->fallback = $fallback;

    return $this;
  }
}
