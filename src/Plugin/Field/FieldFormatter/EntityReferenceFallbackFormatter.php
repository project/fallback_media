<?php

namespace Drupal\fallback_media\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_entity_fallback",
 *   label = @Translation("Rendered entity (with fallback)"),
 *   description = @Translation("Display the referenced entities rendered by entity_view() or a fallback entity if none set."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceFallbackFormatter extends EntityReferenceEntityFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'fallback_entity' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $fallbackEntities = array_map(function ($entity) {
      return $entity->label();
    }, $this->entityTypeManager->getStorage('fallback_entity')->loadMultiple());

    $elements['fallback_entity'] = [
      '#type' => 'select',
      '#options' => $fallbackEntities,
      '#title' => t('Fallback entity'),
      '#default_value' => $fallbackEntities[$this->getSetting('fallback_entity')],
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $fallbackEntities = array_map(function ($entity) {
      return $entity->label();
    }, $this->entityTypeManager->getStorage('fallback_entity')->loadMultiple());
    $fallbackEntity = $this->getSetting('fallback_entity');

    $summary[] = t('Fallback to @entity', ['@entity' => isset($fallbackEntities[$fallbackEntity]) ? $fallbackEntities[$fallbackEntity] : $fallbackEntity]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    if ($items->isEmpty()) {
      $elements[] = $this->fallbackEntityToView($this->getSetting('fallback_entity'), $this->getSetting('view_mode'));
    } else {
      $elements = parent::viewElements($items, $langcode);
    }

    return $elements;
  }

  /**
   * Retrieve the fallback and entity and render.
   */
  protected function fallbackEntityToView($fallbackEntityId, $viewMode) {
    $fallbackEntity = $this->entityTypeManager->getStorage('fallback_entity')->load($fallbackEntityId);
    if (!$fallbackEntity) {
      return [];
    }
    $entity = $this->entityTypeManager->getStorage('media')->load($fallbackEntity->getFallback());
    if (!$entity) {
      return [];
    }
    $view_builder = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId());
    return $view_builder->view($entity, $viewMode, $entity->language()->getId());
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for media entity types.
    $supported_types = [
      'media',
    ];
    $target_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');
    return (in_array($target_type, $supported_types));
  }

}
