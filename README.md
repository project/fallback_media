Fallback Media
==============

Overview
--------

This module allows for a fallback media item to be specified and includes a
field formatter to output the fallback media item if no entity is selected.


Configuration
-------------

* Install the module as you would usually.
* Add a fallback media entity: /admin/structure/fallback_entity
* Edit your content display to use the new *Rendered Entity (with fallback)*
field formatter and select the fallback media entity added above.
